﻿// Middle4.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
using namespace std;
class Fraction
{
public:
    int* numerator ;
    int* denominator ;
	Fraction(int num, int den)
	{
		if (num == 0)
		{
			throw runtime_error("Numerator cannot be zero");
		}
		if (den == 0)
		{
			throw runtime_error("Denominator cannot be zero");
		}
			numerator = new int(num);
			denominator = new int(den);
	}
	
};

int main()
{
	try
	{
		int num, den;
		std::cout << "Enter numerator: ";
		std::cin >> num;
		std::cout << "Enter denominator: ";
		std::cin >> den;

		Fraction frac(num, den);
		std::cout << "Fraction: " << *frac.numerator << "/" << *frac.denominator << std::endl;
	}
	catch (const std::exception& e)
	{
		std::cout << "Invalid input: " << e.what() << std::endl;
	}
}

// Запуск программы: CTRL+F5 или меню "Отладка" > "Запуск без отладки"
// Отладка программы: F5 или меню "Отладка" > "Запустить отладку"

// Советы по началу работы 
//   1. В окне обозревателя решений можно добавлять файлы и управлять ими.
//   2. В окне Team Explorer можно подключиться к системе управления версиями.
//   3. В окне "Выходные данные" можно просматривать выходные данные сборки и другие сообщения.
//   4. В окне "Список ошибок" можно просматривать ошибки.
//   5. Последовательно выберите пункты меню "Проект" > "Добавить новый элемент", чтобы создать файлы кода, или "Проект" > "Добавить существующий элемент", чтобы добавить в проект существующие файлы кода.
//   6. Чтобы снова открыть этот проект позже, выберите пункты меню "Файл" > "Открыть" > "Проект" и выберите SLN-файл.
